#ifndef _MENU_H_
#define _MENU_H_
#include "MenuItem.h"
#include "SimpleMenuItem.h"
#include <vector>
#include <string>
using namespace std;

class Menu : public MenuItem {
public: bool is_root;
	
	public: Menu();
	private: Menu(std::string);
	//public: Menu(std::string,bool);

	public: void addItem(std::string, std::function<void(void)>);
	public: Menu* addSubmenu(std::string);
	
private: vector<MenuItem*> items;
	private: void showMenu();
	private: int getUserInput();
	public: void run();
};

#endif