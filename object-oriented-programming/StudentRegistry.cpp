#include "StudentRegistry.h"

StudentsRegistry * StudentsRegistry::instance = 0;

StudentsRegistry& StudentsRegistry::getInstance() {
	if (!instance) {
		instance = new StudentsRegistry();
	}
	return *instance;
}

bool StudentsRegistry::addStudent(Student& st)
{
	students.push_back(Student(st));
}

Student StudentsRegistry::getStudent(int number)
{
	for (int i = 0; i < students.size; ++i)
	{
		if (i + 1 == number)
		{
			return Student(students[i]);
		}
	}
}

bool StudentsRegistry::removeStudent(int number)
{
	students.erase(students.begin() + number);
}

bool StudentsRegistry::removeStudent(Student st)
{
	vector<Student>::iterator it;
	for (it = students.begin(); it != students.end();)
	{
		if ((it->getFirstName() == st.getFirstName()) && (it->getLastName() == st.getLastName()) && (it->getMiddleName() == st.getMiddleName()) && (it->getGroup() == st.getGroup()))
			it = students.erase(it);
		else
			++it;
	}
}