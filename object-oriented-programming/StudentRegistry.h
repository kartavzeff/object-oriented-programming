#pragma once
#include <iostream>
#include <vector>
#include <cassert>
#include "Student.h"

class StudentsRegistry
{
private:
	static StudentsRegistry* instance;
	vector<Student> students;
	StudentsRegistry() {};
	static StudentsRegistry& getInstance();

public:
	bool addStudent(Student& st);
	bool removeStudent(int number) {};
	bool removeStudent(Student st) {};
	Student getStudent(int number) {};
	int getStudentCount() {};
};