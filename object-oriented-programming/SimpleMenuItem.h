#ifndef _SIMPLE_MENU_ITEM_H_
#define _SIMPLE_MENU_ITEM_H_
#include "MenuItem.h"
#include <functional>

class SimpleMenuItem : public MenuItem {
private: std::function<void(void)> command;
public: void run();
public: SimpleMenuItem(std::string name, std::function<void(void)> command);
};



#endif
