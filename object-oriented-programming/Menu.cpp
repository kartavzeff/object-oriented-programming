#include "Menu.h"
#include <iostream>

Menu::Menu() : MenuItem("") {
	is_root = true;
}

Menu::Menu(std::string title) : MenuItem(title){
	is_root = false;
}

Menu* Menu::addSubmenu(std::string name) {
	Menu* sub =  new Menu(name);
	items.push_back(sub);
	return sub;
}

void Menu::addItem(std::string name, std::function<void(void)> func) {
	SimpleMenuItem* smp = new SimpleMenuItem(name, func);
	items.push_back(smp);
}

void Menu::run() {
	while (1) {
		this->showMenu();
		int index = this->getUserInput();
		if (index == items.size) {
			break;
		}
		this->items[index]->run();
	}
}

void Menu::showMenu() {
	for (int i = 0; i < items.size(); i++) {
		string str = itoa(i, nullptr, 10);
		str += ".   " + items[i]->title() + "/n";
		std::cout << str;
	}
	string str = itoa(items.size, nullptr, 10);
	if (is_root) {
		str += ".  Выход /n";
	}
	else {
		str += ".  Назад /n";
	}
	
	std::cout << str;
}

int Menu::getUserInput() {
	string str;
	std::cout << "Enter selection:  " ;
	std::cin >> str;
	std::cout << "/n";
	return stoi(str);
}