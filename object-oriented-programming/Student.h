#pragma once
#include <string>
#include <map>
class Student
{
private:
	std::string lastName;
	std::string midleName;
	std::string firstName;
	std::string group;
	std::map<std::string, unsigned char> marks;
public:
	Student(std::string firstName, std::string midleName, std::string lastName, std::string group)
	{
		this->firstName = firstName;
		this->midleName = midleName;
		this->lastName = lastName;
		this->group = group;
	}
	std::string getLastName()
	{
		return lastName;
	};
	std::string getMidleName()
	{
		return midleName;
	};
	std::string getFirstName()
	{
		return firstName;
	};
	std::string getGroup()
	{
		return group;
	};
	std::map<std::string, unsigned char> getMarks()
	{
		return marks;
	};

};