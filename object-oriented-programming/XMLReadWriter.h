#include <string>
#include <map>
#include <fstream>
#include "Student.h"
#include <vector>


using namespace std;
class XMLReadWriter
{
public:
	void writeToFile(string filePath, vector<Student> students);
	vector<Student>* readFromFile(string filePath);
	~XMLReadWriter();
	static XMLReadWriter& getInstance();
private:
	static XMLReadWriter * self;
	XMLReadWriter();
	XMLReadWriter(XMLReadWriter const&) = delete;
	XMLReadWriter& operator= (XMLReadWriter const&) = delete;
}; 

XMLReadWriter& XMLReadWriter::getInstance()
{
	if (self != nullptr) return *self;
	else {
		self = new XMLReadWriter();
		return *self;
	}
}

void XMLReadWriter::writeToFile(string filePath, vector<Student> students)
{
	if (students.empty()) throw "net studentov ue***";
	ofstream out;
	out.open(filePath);
	if (!out.is_open()) {
		throw "Cannot open file. chmo\n";
	}
	out << "<students>" << endl;
	for each (Student student in students)
	{
		out << "<Student firstname=" << student.getFirstName()
			<< "\|midleName=" << student.getMidleName()
			<< "\|lastlname=" << student.getLastName()
			<< "\|group=" << student.getGroup() << "\|>" << endl;
		for each (pair<string, unsigned char> subject in student.getMarks())
		{
			out << "<subject name=" << subject.first
				<< "\|mark=" << subject.second << "><\\subject>" << endl;
		}
		out << "<\\Student>" << endl;
	}
	out << "<\\students>" << endl;
	out.close();
}

vector<Student>* XMLReadWriter::readFromFile(string filePath)
{
	vector<Student> * students = new vector<Student>();
	ifstream in;
	in.open(filePath);
	if (!in.is_open()) {
		throw "Cannot open file. chmo\n";
	}
	string line;
	char tmp[100];
	in.getline(tmp,100);
	line = string(tmp);
	Student * student = nullptr;
	while (!line.empty())
	{
		
		if (line.substr(0, 9) == "<Student ") {
			int pos = line.find('\|');
			string firstName = line.substr(string("<Student firstname=").length(), pos - string("<Student firstname=").length());
			int prevPos = pos;
			pos = line.find('\|', prevPos + 1);
			string midleName = line.substr(prevPos + string("\|midleName=").length(), pos - prevPos - string("\|midleName=").length());
			prevPos = pos;
			pos = line.find('\|', prevPos + 1);
			string lastlname = line.substr(prevPos + string("\|lastlname=").length(), pos - prevPos - string("\|lastlname=").length());
			prevPos = pos;
			pos = line.find('\|', prevPos + 1);
			string group = line.substr(prevPos + string("\|group=").length(), pos - prevPos - string("\|group=").length());
			student = new Student(firstName, midleName, lastlname, group);
		}
		if (line.substr(0, 11) == "<subject  ") {
			if (student == nullptr) continue;
			int pos = line.find('\|');
			string subject = line.substr(15, pos - 15);
			string _mark = line.substr(pos + string("\|mark=").length(), 1);
			unsigned char mark = _mark[0];
			student->getMarks().insert(pair<string, unsigned char>(subject, mark));
		}
		if (line.substr(0, 11) == "<\\Student>")students->push_back(*student);

		char tmp[100];
		in.getline(tmp, 100);
		line = string(tmp);
	}
	in.close();
	return students;
}

XMLReadWriter::XMLReadWriter()
{

}

XMLReadWriter::~XMLReadWriter()
{
}
