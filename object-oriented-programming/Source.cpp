#include "XMLReadWriter.h"

XMLReadWriter *XMLReadWriter::self = nullptr;

int main()
{
	XMLReadWriter & readWriter = XMLReadWriter::getInstance();
	vector <Student> students;
	students.push_back(Student("A1", "A2", "A3", "B"));
	students.push_back(Student("A11", "A12", "A13", "B1"));
	readWriter.writeToFile("test.txt", students);
	printf("wrote");
	vector <Student> * students2 = readWriter.readFromFile("test.txt");
	printf("wrote:");
	return 0;
}