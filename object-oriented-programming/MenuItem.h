#ifndef _MENUITEM_H_
#define _MENUITEM_H_
#include <string>

class MenuItem
{
	std::string name;

public:
	
	MenuItem(std::string name);
	
	std::string title();
	
	virtual void run() = 0;
};

#endif