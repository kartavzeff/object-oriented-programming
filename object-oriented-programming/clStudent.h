#pragma once
#include <iostream>
#include <string>
#include <map>
#include <vector>

using namespace std;

class Student {
private:
	string first_name;
	string middle_name;
	string last_name;
	string group;
	map<string, int> marks;
public:
	Student(string firstName, string middleName, string lastName, string students_group, vector<string>  subjects, vector<int> marks);
	Student(const Student &st);
	~Student() {};
	bool setFirstName(string firstName);
	bool setMiddleName(string middleName);
	bool setLastName(string lastName);
	bool setGroup(string group);
	string getFirstName();
	string getMiddleName();
	string getLastName();
	string getGroup();
};