#include "Student.h"
#include <cassert>

Student::Student(string firstName, string middleName, string lastName, string students_group, vector<string>  subjects, vector<int> _marks)
{
	first_name = firstName;
	middle_name = middleName;
	last_name = lastName;
	group = students_group;
	if (subjects.size == _marks.size) {
		for (unsigned int i = 0; i < subjects.size; ++i)
			marks.insert(pair<string, int>(subjects[i], _marks[i]));
	}
	else {
		exception("Size of subjects != size of marks");
	}
}

Student::Student(const Student &st)
{
	first_name = st.first_name;
	middle_name = st.middle_name;
	last_name = st.last_name;
	group = st.group;
	marks = st.marks;
}

bool Student::setFirstName(string firstName)
{
	try {
		this->first_name = firstName;
		return true;
	}
	catch (...) {
		cout << "Can not assign" << endl;
		return false;
	}
}

bool Student::setMiddleName(string middleName)
{
	try {
		this->middle_name = middleName;
		return true;
	}
	catch (...) {
		cout << "Can not assign" << endl;
		return false;
	}
}

bool Student::setLastName(string lastName)
{
	try {
		this->last_name = lastName;
		return true;
	}
	catch (...) {
		cout << "Can not assign" << endl;
	}

	return false;
}

bool Student::setGroup(string students_group)
{
	try {
		this->group = students_group;
		return true;
	}
	catch (...) {
		cout << "Can not assign" << endl;
	}

	return false;
}

string Student::getFirstName()
{
	return this->first_name;
}

string Student::getMiddleName()
{
	return this->middle_name;
}

string Student::getLastName()
{
	return this->last_name;
}

string Student::getGroup()
{
	return this->group;
}