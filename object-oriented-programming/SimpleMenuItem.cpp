#include "SimpleMenuItem.h"
#include "MenuItem.h"
#include <iostream>
#include <string>

void SimpleMenuItem::run() {
	this->command();
}

SimpleMenuItem::SimpleMenuItem(std::string name, std::function<void(void)> _command): MenuItem(name) {
	this->command = _command;
}